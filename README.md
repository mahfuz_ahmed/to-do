##Spring MVC Demo Todo

This is a simple to-do web application, based on Spring MVC, implemented with the purpose of demonstrating Spring xml 
configuration with some best practices.

####Framework & Technology:
- Hibernate
- Spring MVC
- SiteMesh
- JSP
- Bootstrap
- Embedded Tomcat
- Gradle

####Steps to deploy this project:
- Clone this repo
- Build using the command: gradle clean build
- Start Tomcat using the command: gradle tomcatRun
